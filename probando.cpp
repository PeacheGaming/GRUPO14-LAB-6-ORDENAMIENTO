#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;

struct Nodo{
    int dato;
    Nodo *sgte,*ant;
};
int Total=0;
typedef Nodo *Tlista;
void limpiarBuffer();
void menu(Tlista &l1);
void mostrar(Tlista l1);
Tlista crearNodo(int valor){
    Tlista aux=new(Nodo);
    aux->ant=NULL;
    aux->dato=valor;
    aux->sgte=NULL;
}
Tlista seleccion(Tlista l1,int i){
     for(int j=0;j<i;j++)
        l1=l1->sgte;
     return l1;
}
void InsertarFinal(Tlista &l1,int valor){
    Tlista aux=crearNodo(valor);
    Tlista temp=l1;
    if(l1==NULL)
        l1=aux;
    else{
        while(temp->sgte!=NULL)
            temp=temp->sgte;
        temp->sgte=aux;
        aux->ant=temp;
    }
    Total++;
}
void shell(Tlista &l1){
   int salto, aux, i;
   boolean cambios;
   for(salto=Total; salto!=0; salto/=2){
       cambios=true;
       while(cambios){
           cambios=false;
           for(i=salto; i<Total; i++)
                   if(seleccion(l1,i-salto)->dato>seleccion(l1,i)->dato){
                     aux=seleccion(l1,i)->dato;
                     seleccion(l1,i)->dato=seleccion(l1,i-salto)->dato;
                     seleccion(l1,i-salto)->dato=aux;
                     cambios=true;
                   }
            }
        }
}

int hallarCant(Tlista inic,Tlista fin){
    if(inic==NULL || fin==NULL){
        return 0;
    }
    else{
        int n=1;
        while(inic!=fin){
            n++;
            inic=inic->sgte;
        }
        return n;
    }
}

Tlista hallarPivote(Tlista inic, Tlista fin, int tam){
    int pos = tam/2;
    Tlista pipa=inic;
    for(int i=1;i<pos;i++){
        pipa=pipa->sgte;
    }
    return pipa;
}
/*
void quickSort(Tlista inic,Tlista fin){
    int tam=hallarCant(inic,fin);
    if(tam>=2){
            bool aux; int x;
            Tlista gizq=inic, gder=fin;
            Tlista pivote=hallarPivote(inic,fin,tam), pivoteinic = pivote;
            while(gizq!=pivoteinic){
                if(gizq->dato>pivote->dato){
                    aux=false;
                    while(gder!=pivoteinic && aux==true){
                        if(gder->dato<gizq->dato){
                            aux=true;
                            x=gder->dato;
                            gder->dato=gizq->dato;
                            gizq->dato=x;
                        }
                    }
                    if(aux==false){
                        x=gizq->dato;
                        gizq->dato=pivote->dato;
                        pivote->dato=x;
                        pivote=gizq;
                    }
                }
            }
            while(gder!=pivoteinic){
                if(gder->dato<pivote->dato){
                    x=gder->dato;
                    gder->dato=pivote->dato;
                    pivote->dato=x;
                    pivote=gder;
                }
            }
            quickSort(inic,pivote->ant);
            quickSort(pivote->sgte,fin);
    }
}
void QuickSort(Tlista &l1){
	quickSort(l1,seleccion(l1,Total));
}*/
//
int particion(Tlista &l1, int izq, int der){
      int i = izq, j = der;
      int tmp;
      int pivot = seleccion(l1,(izq+der)/2)->dato;

      while (i <= j) {
            while (seleccion(l1,i)->dato < pivot)
                  i++;
            while (seleccion(l1,j)->dato > pivot)
                  j--;
            if (i <= j) {
                  tmp = seleccion(l1,i)->dato;
                  seleccion(l1,i)->dato = seleccion(l1,j)->dato;
                  seleccion(l1,j)->dato = tmp;
                  i++;
                  j--;
            }
      }

      return i;
}
void quickSort(Tlista &l1, int izq, int der) {
      int indice = particion(l1, izq,der);
      if (izq< indice - 1)
            quickSort(l1, izq, indice - 1);
      if (indice < der)
            quickSort(l1, indice, der);
}
void QuickSort(Tlista &l1){
   quickSort(l1,0,Total-1);
}
Tlista rellenar(){
    int valor;
    Tlista aux=NULL;
    for(int i=0;i<6;i++){
        valor=rand()%20;
        InsertarFinal(aux,valor);
    }
    return aux;
}
void mostrar(Tlista l1){
     cout<<"\t\t\t";
     while(l1!=NULL){
        cout<<l1->dato;
        if(l1->sgte!=NULL)
            cout<<"->";
        l1=l1->sgte;
     }
     system("pause>nul");
}
void opcion1(Tlista &l1){
     int valor;
     cout<<"\t\t\tIngrese valor : "; cin>>valor;
     if(cin.fail()){
        cout<<"ERROR ! DATO INVALIDO"<<endl;
        limpiarBuffer();
        opcion1(l1);
     }else{
        InsertarFinal(l1,valor);
        menu(l1);
     }
}
void menu(Tlista &l1){
	int p;
	system("cls");
	cout << "\t\t\t浜様様様様様様様様様様様様様様様様様融" << endl;
	cout << "\t\t\t�                MENU                �" << endl;
	cout << "\t\t\t麺様様様様様様様様様様様様様様様様様郵" << endl;
	cout << "\t\t\t�  1. Insertar final                 �" << endl;
	cout << "\t\t\t�  2. Ordenar Por Shell Sort         �" << endl;
	cout << "\t\t\t�  3. Ordenar Por Quick Sort         �" << endl;
	cout << "\t\t\t�  4. Mostrar                        �" << endl;
	cout << "\t\t\t�  5. Salir                          �" << endl;
	cout << "\t\t\t藩様様様様様様様様様様様様様様様様様夕" << endl;
	cout << "\t\t\t\tIngrese una opcion: ";	cin>>p;
	switch (p){
	case 1: opcion1(l1); menu(l1); break;
	case 2: shell(l1); menu(l1); break;
	case 3: QuickSort(l1); menu(l1); break;
	case 4:	 mostrar(l1); menu(l1); break;
	case 5: system("exit");
	default: cout << "Opcion invalida!\n";
		limpiarBuffer();
		menu(l1); break;
	}
}
void limpiarBuffer(){
	system("pause > nul");
	std::cin.clear();
	std::cin.ignore(1000, '\n');
}
int main(){
	Tlista l1=rellenar();
    menu(l1);
}
